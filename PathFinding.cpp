#include "PathFinding.h"

#include <iostream>
#include <limits>

void PopulateNeighbours(Location *targetLocation, Location *startLocation,
                        const unsigned char *map, bool *startPointFound,
                        std::list<int> *parentIds);

bool CreateGraphAndValidateStartPointIsPartOfGraph(Location *targetLocation, Location *startLocation,
                                                   const unsigned char *map);

void SearchPaths(Location *currentLocation, Location *startLocation,
                 std::list<std::list<int> *> *foundPaths,
                 std::list<int> *currentPath, unsigned int maxPathDepth);

int FindPath(const int nStartX, const int nStartY,
             const int nTargetX, const int nTargetY,
             const unsigned char *pMap, const int nMapWidth, const int nMapHeight,
             int *pOutBuffer, const int nOutBufferSize) {
    int result = RETURN_CODE_NO_PATHS_FOUND;

    Location *startLocation = new Location(nStartX, nStartY, nMapWidth, nMapHeight);
    Location *targetLocation = new Location(nTargetX, nTargetY, nMapWidth, nMapHeight);

    // start by building a graph from the target location onward so you can eliminate the need to check
    // multiple unconnected graphs since the target location must be part of the result.
    if (CreateGraphAndValidateStartPointIsPartOfGraph(targetLocation, startLocation, pMap)) {
        // the targetLocation is now the start point of a graph connecting all reachable points of the map
        // we can now find the shortest path in this graph with breath first recursive traversal of the graph until
        // we hit the startNode

        // Note: I am no expert in path finding algorithms. For the short research I have done this is one of the
        // slower ways to solve the problem. So this is not the way I would tackle a real world example.

        // Recursivly dive into the graph - whenever we find a complete path to the start location we store
        // it in a list. wont look into paths that are longer than nOutBufferSize
        std::list<std::list<int> *> foundPaths;
        std::list<int> currentPath;
        SearchPaths(targetLocation, startLocation, &foundPaths, &currentPath, nOutBufferSize);

        // Choose the shortest path from the result list
        int minimumPathLength = std::numeric_limits<int>::max();
        std::list<int> *chosenPath = nullptr;
        for (auto const &path : foundPaths) {
            int currentPathLength = path->size();
            if (currentPathLength < minimumPathLength) {
                minimumPathLength = currentPathLength;
                chosenPath = path;
            }
        }

        if (chosenPath) {
            // reverse the path since we created it from the endpoint
            chosenPath->reverse();

            // store it in the output buffer
            int outputIndex = 0;
            for (auto const &locationIndex : *chosenPath) {
                pOutBuffer[outputIndex] = locationIndex;
                outputIndex++;
            }

            result = minimumPathLength;
        }

        // cleanup paths on heap
        for (auto const &path : foundPaths) {
            delete path;
        }
    }

    delete startLocation;
    delete targetLocation;

    return result;
}

void SearchPaths(Location *currentLocation, Location *startLocation,
                 std::list<std::list<int> *> *foundPaths,
                 std::list<int> *currentPath, const unsigned int maxPathDepth) {
    // maximum allowed path depth reached - we end the search with no result found
    if (currentPath->size() == maxPathDepth) {
        return;
    }

    if (currentLocation->equals(startLocation) && !currentPath->empty()) {
        foundPaths->push_back(new std::list<int>(*currentPath));
        return;
    }

    currentPath->push_back(currentLocation->index());
    for (auto const &neighbour : currentLocation->neighbours) {
        std::list<int> newPathStorage(*currentPath);
        SearchPaths(neighbour, startLocation, foundPaths, &newPathStorage, maxPathDepth);
    }
}


bool CreateGraphAndValidateStartPointIsPartOfGraph(Location *targetLocation, Location *startLocation,
                                                   const unsigned char *map) {
    bool startPointFound = false;
    std::list<int> parentIdTrail;

    PopulateNeighbours(targetLocation, startLocation, map, &startPointFound, &parentIdTrail);

    return startPointFound;
}

void PopulateNeighbours(Location *targetLocation, Location *startLocation,
                        const unsigned char *map, bool *startPointFound,
                        std::list<int> *parentIdTrail) {
    if (targetLocation->equals(startLocation)) {
        *(startPointFound) = true;
        // we could optimize and quit here, but for completeness sake I want to have the full graph
    }

    const bool isPartOfParentTrail = std::find(parentIdTrail->begin(), parentIdTrail->end(), targetLocation->index()) !=
                                     parentIdTrail->end();
    if (!isPartOfParentTrail) {
        // mark this location as visited before recursions so that they do not go to an endless loop
        parentIdTrail->push_back(targetLocation->index());

        if (targetLocation->leftNeighbourIndex() >= 0) {
            Location *leftNeighbour = new Location(targetLocation->x - 1, targetLocation->y, targetLocation->gridWidth,
                                                   targetLocation->gridHeight);
            // map values are 1 (true) if traversable and 0 when not - use that directly in if statement
            if (map[leftNeighbour->index()]) {
                targetLocation->addNeighbour(leftNeighbour);
                // recurse into all neighbours
                std::list<int> parentIdListCopy(*parentIdTrail);
                PopulateNeighbours(leftNeighbour, startLocation, map, startPointFound, &parentIdListCopy);
            }
        }
        if (targetLocation->rightNeighbourIndex() >= 0) {
            Location *rightNeighbour = new Location(targetLocation->x + 1, targetLocation->y, targetLocation->gridWidth,
                                                    targetLocation->gridHeight);
            // map values are 1 (true) if traversable and 0 when not - use that directly in if statement
            if (map[rightNeighbour->index()]) {
                targetLocation->addNeighbour(rightNeighbour);
                // recurse into all neighbours
                std::list<int> parentIdListCopy(*parentIdTrail);
                PopulateNeighbours(rightNeighbour, startLocation, map, startPointFound, &parentIdListCopy);
            }
        }
        if (targetLocation->topNeighbourIndex() >= 0) {
            Location *topNeighbor = new Location(targetLocation->x, targetLocation->y - 1, targetLocation->gridWidth,
                                                 targetLocation->gridHeight);
            // map values are 1 (true) if traversable and 0 when not - use that directly in if statement
            if (map[topNeighbor->index()]) {
                targetLocation->addNeighbour(topNeighbor);
                // recurse into all neighbours
                std::list<int> parentIdListCopy(*parentIdTrail);
                PopulateNeighbours(topNeighbor, startLocation, map, startPointFound, &parentIdListCopy);
            }
        }
        if (targetLocation->bottomNeighbourIndex() >= 0) {
            Location *bottomNeighbour = new Location(targetLocation->x, targetLocation->y + 1,
                                                     targetLocation->gridWidth,
                                                     targetLocation->gridHeight);
            // map values are 1 (true) if traversable and 0 when not - use that directly in if statement
            if (map[bottomNeighbour->index()]) {
                targetLocation->addNeighbour(bottomNeighbour);
                // recurse into all neighbours
                std::list<int> parentIdListCopy(*parentIdTrail);
                PopulateNeighbours(bottomNeighbour, startLocation, map, startPointFound, &parentIdListCopy);
            }
        }
    }
}

Location *Location::addNeighbour(Location *neighbour) {
    this->neighbours.push_back(neighbour);
    return this;
}

Location::Location(int x, int y, int gridWidth, int gridHeight) : x(x), y(y) {
    this->gridWidth = gridWidth;
    this->gridHeight = gridHeight;
    this->mapSize = gridWidth * gridHeight;
}

Location::~Location() {
    // free memory occupied by neighbour Locations
    for (auto const &neighbour : this->neighbours) {
        delete neighbour;
    }
    this->neighbours.clear();
}

int Location::bottomNeighbourIndex() {
    if (y + 1 < gridHeight) {
        return (y + 1) * gridWidth + x;
    }

    return -1;
}

int Location::topNeighbourIndex() {
    if (y - 1 >= 0) {
        return (y - 1) * gridWidth + x;
    }

    return -1;
}

int Location::rightNeighbourIndex() {
    if (x + 1 < gridWidth) {
        return y * gridWidth + x + 1;
    }

    return -1;
}

int Location::leftNeighbourIndex() {
    if (x - 1 >= 0) {
        return y * gridWidth + x - 1;
    }

    return -1;
}

int Location::index() {
    return y * gridWidth + x;
}

bool Location::equals(Location *otherLocation) {
    return this->index() == otherLocation->index();
}

bool Location::hasNoNeighbours() {
    return this->neighbours.empty();
}
