#ifndef PARADOXPATH_PATHFINDING_H
#define PARADOXPATH_PATHFINDING_H

#include <list>

static const int RETURN_CODE_NO_PATHS_FOUND = -1;

class Location {
public:
    int x;
    int y;
    int gridHeight;
    int gridWidth;
    int mapSize;

    std::list<Location *> neighbours;

    Location(int x, int y, int gridWidth, int gridHeight);

    virtual ~Location();

    int index();

    int leftNeighbourIndex();

    int rightNeighbourIndex();

    int topNeighbourIndex();

    int bottomNeighbourIndex();

    Location *addNeighbour(Location *neighbour);

    bool equals(Location *otherLocation);

    bool hasNoNeighbours();
};

int FindPath(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char *pMap,
             const int nMapWidth, const int nMapHeight, int *pOutBuffer, const int nOutBufferSize);

#endif //PARADOXPATH_PATHFINDING_H
