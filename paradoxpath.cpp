﻿#include <iostream>
#include <string>
#include "PathFinding.h"

bool MapToLocationWith3By3MapAnd00CoordinatesReturnsLocationWithIndex0() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(0, 0, 3, 3);

    return result->index() == 0;
}

bool MapToLocationWith3By3MapAnd11CoordinatesReturnsLocationWithIndex4() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(1, 1, 3, 3);

    return result->index() == 4;
}

bool MapToLocationWith3By3MapAnd22CoordinatesReturnsLocationWithIndex8() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(2, 2, 3, 3);

    return result->index() == 8;
}

bool NeighbourIndexesWith3By3MapAnd11CoordinatesReturnsCorrectValues() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(1, 1, 3, 3);

    return result->leftNeighbourIndex() == 3 && result->rightNeighbourIndex() == 5 && result->topNeighbourIndex() == 1
           && result->bottomNeighbourIndex() == 7;
}

bool NeighbourIndexesWith3By3MapAnd22CoordinatesReturnsCorrectValues() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(2, 2, 3, 3);

    return result->leftNeighbourIndex() == 7 && result->rightNeighbourIndex() == -1 && result->topNeighbourIndex() == 5
           && result->bottomNeighbourIndex() == -1;
}

bool NeighbourIndexesWith3By3MapAnd00CoordinatesReturnsCorrectValues() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(0, 0, 3, 3);

    return result->leftNeighbourIndex() == -1 && result->rightNeighbourIndex() == 1 && result->topNeighbourIndex() == -1
           && result->bottomNeighbourIndex() == 3;
}

bool NeighbourIndexesWith3By3MapAnd02CoordinatesReturnsCorrectValues() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(0, 2, 3, 3);

    return result->leftNeighbourIndex() == -1 && result->rightNeighbourIndex() == 7 && result->topNeighbourIndex() == 3
           && result->bottomNeighbourIndex() == -1;
}

bool NeighbourIndexesWith3By3MapAnd20CoordinatesReturnsCorrectValues() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};

    Location *result = new Location(2, 0, 3, 3);

    return result->leftNeighbourIndex() == 1 && result->rightNeighbourIndex() == -1 && result->topNeighbourIndex() == -1
           && result->bottomNeighbourIndex() == 5;
}

bool FindPathWithExistingPathReturnsPath() {
    unsigned char pMap[] = {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1};
    int pOutBuffer[12];
    int numberOfSteps = FindPath(0, 0, 1, 2, pMap, 4, 3, pOutBuffer, 12);

    return numberOfSteps == 3 && pOutBuffer[0] == 1 && pOutBuffer[1] == 5 && pOutBuffer[2] == 9;
}

bool FindPathWithNonExistingPathReturnsMinusOne() {
    unsigned char pMap[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};
    int pOutBuffer[7];
    int numberOfSteps = FindPath(2, 0, 0, 2, pMap, 3, 3, pOutBuffer, 7);

    return numberOfSteps == RETURN_CODE_NO_PATHS_FOUND;
}

void RunTest(const std::string &testName, bool (*testFunction)()) {
    std::cout << testName << ": ";
    if (testFunction()) {
        std::cout << "SUCCESS";
    } else {
        std::cout << "FAILURE";
    }
    std::cout << std::endl;
}

int main() {
    RunTest("MapToLocationWith3By3MapAnd00CoordinatesReturnsLocationWithIndex0",
            &MapToLocationWith3By3MapAnd00CoordinatesReturnsLocationWithIndex0);
    RunTest("MapToLocationWith3By3MapAnd11CoordinatesReturnsLocationWithIndex4",
            &MapToLocationWith3By3MapAnd11CoordinatesReturnsLocationWithIndex4);
    RunTest("MapToLocationWith3By3MapAnd22CoordinatesReturnsLocationWithIndex8",
            &MapToLocationWith3By3MapAnd22CoordinatesReturnsLocationWithIndex8);
    RunTest("NeighbourIndexesWith3By3MapAnd11CoordinatesReturnsCorrectValues",
            &NeighbourIndexesWith3By3MapAnd11CoordinatesReturnsCorrectValues);
    RunTest("NeighbourIndexesWith3By3MapAnd22CoordinatesReturnsCorrectValues",
            &NeighbourIndexesWith3By3MapAnd22CoordinatesReturnsCorrectValues);
    RunTest("NeighbourIndexesWith3By3MapAnd00CoordinatesReturnsCorrectValues",
            &NeighbourIndexesWith3By3MapAnd00CoordinatesReturnsCorrectValues);
    RunTest("NeighbourIndexesWith3By3MapAnd02CoordinatesReturnsCorrectValues",
            &NeighbourIndexesWith3By3MapAnd02CoordinatesReturnsCorrectValues);
    RunTest("NeighbourIndexesWith3By3MapAnd20CoordinatesReturnsCorrectValues",
            &NeighbourIndexesWith3By3MapAnd20CoordinatesReturnsCorrectValues);
    RunTest("FindPathWithExistingPathReturnsPath", &FindPathWithExistingPathReturnsPath);
    RunTest("FindPathWithNonExistingPathReturnsMinusOne", &FindPathWithNonExistingPathReturnsMinusOne);

//    system("pause");
    return 0;
}
